<?php
namespace tests\functional;


class AddCartCest
{
    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testAddExistsProduct(\FunctionalTester $i): void
    {
       $i->amOnPage('/');

        $productId = 5;

       $i->sendAjaxPost('/cart/add',[
           'product' => $productId
       ]);

       $result = json_decode($i->grabResponse(),1);

       $i->assertArrayHasKey('error',$result);

       $i->assertFalse($result['error']);

       $i->amOnPage('/');

       $i->see('in cart', 'a[data-id=' . $productId .']');
    }

    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testAddNotExistsProduct(\FunctionalTester $i): void
    {
        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[
            'product' => 999,
        ]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertArrayHasKey('error',$result);

        $i->assertTrue($result['error']);
    }

    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testAddWithoutProduct(\FunctionalTester $i): void
    {
        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[
            'product' => '',
        ]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertArrayHasKey('error',$result);

        $i->assertTrue($result['error']);
    }

    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testAddNoProduct(\FunctionalTester $i): void
    {
        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertArrayHasKey('error',$result);

        $i->assertTrue($result['error']);
    }

    /**
     * @param \FunctionalTester $i
     * @throws \Exception
     */
    public function testAddNoCsrf(\FunctionalTester $i): void
    {
        $i->amOnPage('/');

        $i->sendAjaxPost('/cart/add',[
            '_csrf' => ''
        ]);

        $result = json_decode($i->grabResponse(),1);

        $i->assertNull($result);
    }
}